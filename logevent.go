// Command logevent captures data from stdin or runs command, capturing its
// stdout/stderr and sends this data to dashboard
package main

import (
	"bytes"
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"time"

	"bitbucket.org/artyom_p/logevent/internal"

	"github.com/artyom/autoflags"
)

const maxPayloadSize = 64 << 10

func main() {
	args := args{
		User: userVar,
		Pass: passVar,
		Dst:  dstVar,
	}
	autoflags.Define(&args)
	flag.Parse()
	args.FromEnv()
	if !args.Valid() {
		flag.Usage()
		os.Exit(1)
	}
	for _, v := range []string{"LOGEVENT_USER", "LOGEVENT_PASS", "LOGEVENT_DST"} {
		os.Unsetenv(v)
	}
	var b []byte
	var err error
	if !IsTerminal() && len(flag.Args()) == 0 {
		b, err = slurpStdin()
	} else {
		b, err = runcmd(flag.Args())
	}
	err2 := upload(args.Dst, args.User, args.Pass, b)
	if err2 != nil {
		os.Stdout.Write(b)
		fmt.Fprintln(os.Stderr, err2)
	}
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	if err != nil || err2 != nil {
		os.Exit(1)
	}
}

func runcmd(s []string) ([]byte, error) {
	if len(s) == 0 {
		return nil, errors.New("nothing to run")
	}
	cmd := exec.Command(s[0], s[1:]...)
	w := &internal.PrefixSuffixSaver{N: maxPayloadSize / 2}
	cmd.Stdout = w
	cmd.Stderr = w
	err := cmd.Run()
	return w.Bytes(), err
}

func slurpStdin() ([]byte, error) {
	w := &internal.PrefixSuffixSaver{N: maxPayloadSize / 2}
	_, err := io.Copy(w, os.Stdin)
	return w.Bytes(), err
}

func upload(dst, user, pass string, b []byte) error {
	if len(b) == 0 {
		return nil
	}
	req, err := http.NewRequest(http.MethodPost, dst, bytes.NewReader(b))
	if err != nil {
		return err
	}
	req.SetBasicAuth(user, pass)
	if name, err := os.Hostname(); err == nil {
		req.Header.Set("X-Hostname", name)
	}
	ctx, _ := context.WithTimeout(context.Background(), time.Minute)
	req = req.WithContext(ctx)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusNoContent {
		return nil
	}
	return fmt.Errorf("unexpected response code: %q", resp.Status)
}

// IsTerminal returns true if stdin is attached to terminal (it's a device file).
func IsTerminal() bool {
	st, err := os.Stdin.Stat()
	if err != nil {
		return false
	}
	return st.Mode()&os.ModeDevice != 0
}

type args struct {
	User string `flag:"u,username"`
	Pass string `flag:"p,password"`
	Dst  string `flag:"dst,destination url"`
}

func (a *args) FromEnv() {
	if a.User == userVar {
		a.User = os.ExpandEnv(a.User)
	}
	if a.Pass == passVar {
		a.Pass = os.ExpandEnv(a.Pass)
	}
	if a.Dst == dstVar {
		a.Dst = os.ExpandEnv(a.Dst)
	}
}

const (
	userVar = `${LOGEVENT_USER}`
	passVar = `${LOGEVENT_PASS}`
	dstVar  = `${LOGEVENT_DST}`
)

func (a *args) Valid() bool {
	return a.User != "" && a.Pass != "" && a.Dst != "" &&
		a.User != userVar && a.Pass != passVar && a.Dst != dstVar
}
